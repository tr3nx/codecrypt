<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

	public static function boot()
	{
		parent::boot();

		static::creating(function (Blog $blog) {
			$blog->slug = str_slug($blog->title);
		});
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

}
