<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PostBlogRequest extends Request
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return $this->auth()->check();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'author' => 'required|integer|exists:users,id',
			'title' => 'required|between:2,200',
			'content' => 'required|min:1'
		];
	}
}
