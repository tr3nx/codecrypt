<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateBlogRequest extends Request
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return $this->auth()->check();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'author' => 'integer|exists:user,id',
			'slug' => 'min:1'
			'title' => 'between:2,200',
			'content' => 'min:1'
		];
	}
}
