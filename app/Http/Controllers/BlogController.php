<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\PostBlogRequest;
use App\Http\Requests\UpdateBlogRequest;

use App\Blog;

use GrahamCampbell\Markdown\Facades\Markdown;

class BlogController extends Controller
{
	public function __construct()
	{
		// $this->middleware(['auth', ['except' => ['index', 'show']]]);
	}

	public function index()
	{
		$blogs = Blog::where('published', true)->get();

		return view('blog.index', compact('blogs'));
	}

	public function show($slug)
	{
		$blog = Blog::where('slug', $slug)->where('published', true)->firstOrFail();

		$blog->html = Markdown::convertToHtml($blog->content);

		return view('blog.show', compact('blog'));
	}

	public function edit($id)
	{
		$blog = Blog::where('id', $id)->firstOrFail();

		return view('blog.edit', compact('blog'));
	}

	public function store(PostBlogRequest $request)
	{
		$blog = Blog::create([
			'author' => $request->auth()->user()->id,
			'title' => $request->get('title'),
			'content' => $request->get('content'),
			'published' => true
		]);

		return redirect()->route('blog.show', [$blog->id]);
	}

	public function update(UpdateBlogRequest $request, $id)
	{
		$blog = Blog::where('id', $id)->firstOrFail();

		$update = [];

		if ($request->has('title') && $request->get('title') != $blog->title) {
			$update['title'] = $request->get('title');
		}

		if ($request->has('slug') && $request->get('slug') != $blog->slug) {
			$update['slug'] = $request->get('slug');
		}

		if ($request->has('author') && $request->get('author') != $blog->author) {
			$update['author'] = $request->get('author');
		}

		if ($request->has('content') && $request->get('content') != $blog->content) {
			$update['content'] = $request->get('content');
		}

		if (count($update)) {
			$blog->update($update);

			$blog->save();
		}

		return redirect()->route('blog.edit', [$blog->id]);
	}

}
