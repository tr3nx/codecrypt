<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Blog;

class PagesController extends Controller
{
	public function welcome()
	{
		$blogs = Blog::where('published', true)->with('user')->get();

		return view('pages.welcome', compact('blogs'));
	}

}
