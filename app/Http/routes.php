<?php

Route::group(['middleware' => 'web'], function () {
	Route::get('home', ['as' => 'home', 'uses' => 'PagesController@welcome']);
	Route::get('{slug}', ['as' => 'blog.show', 'uses' => 'BlogController@show']);
	Route::get('latest', ['as' => 'blog.index', 'uses' => 'BlogController@index']);
	Route::get('blogs', ['as' => 'blog.index', 'uses' => 'BlogController@index']);
	Route::post('post', ['as' => 'blog.store', 'uses' => 'BlogController@store']);
	Route::get('blogs/{id}/edit', ['as' => 'blog.edit', 'uses' => 'BlogController@edit']);
	Route::post('blogs/{id}', ['as' => 'blog.update', 'uses' => 'BlogController@update']);

	Route::auth();

	Route::get('/', ['as' => 'welcome', 'uses' => 'PagesController@welcome']);
});