<section class="header">
	<div class="container">
		<div class="logo">
			<a href="/" title="CodeCrypt">CodeCrypt</a>
		</div>
		<ul class="nav-primary">
			<li><a href="#">Latest</a></li>
			<li><a href="#">Archive</a></li>
		</ul>
	</div>
</section>
<div class="title-wrapper">
	<div class="title">
		<h1>Welcome to CodeCrypt.io</h1>
	</div>
</div>