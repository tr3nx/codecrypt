<section class="footer">
	<div class="container">
		<div class="copyright">
			&copy; Copyright <a href="">tr3nx</a> <script>(function() { document.write(new Date().getFullYear()); })();</script>
		</div>
		<div class="topside">
			<a href="#" data-back-to-top>Top</a>
		</div>
	</div>
</section>
@include('layouts.partials.javascript')