<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js"></script>

<script src="/assets/js/highlight.pack.js"></script>

<script>
$(document).ready(function() {

	$('body').each(function() {
		$(this).css("opacity", 1);
	});
	
	hljs.initHighlighting();

	$('[data-back-to-top]').on('click', function(e) {
		e.preventDefault();
		$("html, body").animate({ scrollTop: 0 }, "slow");
	});
	
	$(window).scroll(function() {
		var scrollAmount = $(window).scrollTop();
		var headerContainer = $('section.header');

		if (scrollAmount >= 100) {
			if ( ! headerContainer.hasClass('scroll')) {
				headerContainer.addClass('scroll');
			}
		} else if (scrollAmount < 100) {
			if (headerContainer.hasClass('scroll')) {
				headerContainer.removeClass('scroll');
			}
		}

		if (scrollAmount >= 250) {
			if ( ! headerContainer.hasClass('up')) {
				headerContainer.addClass('up');
			}
		} else {
			if (headerContainer.hasClass('up')) {
				headerContainer.removeClass('up');
			}
		}
	});

	console.log('[+] CodeCrypt Loaded!');
});
</script>