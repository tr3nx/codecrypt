
                    @if (Auth::guest())
                    @else
                    {{ Auth::user()->name }}
                    @endif


<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="tr3nx">

		<title>CodeCrypt</title>

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/4.1.1/normalize.min.css">
		<link rel="stylesheet" href="/assets/css/styles/zenburn.css">
		<link rel="stylesheet" href="/assets/css/app.css">

		<link rel="shortcut icon" href="/favicon.ico">

		<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
	</head>
	<body>
		@include('layouts.partials.header')

		<div id="page">
			@yield('content')
		</div>

		@include('layouts.partials.footer')
	</body>
</html>