@extends('layouts.app')
@section('content')
	<section class="content">
		<div class="container">
			<ul class="index">
				@foreach ($blogs as $blog)
				<li>
					<a href="{!! route('blog.show', [$blog->slug]) !!}">{{ $blog->title }}</a>
					<span class="author">by {{ $blog->user->name }}</span> <span class="when">{{ $blog->created_at->diffForHumans() }}</span>
				</li>
				@endforeach
			</ul>
		</div>
	</section>
@endsection