@extends('layouts.app')

@section('content')
	<h1>Blog Posts</h1>
	<ul>
	@foreach ($blogs as $blog)
		<li><a href="{{ route('blog.show', [$blog->slug]) }}">{{$blog->title}}</a></li>
	@endforeach
	</ul>
@endsection